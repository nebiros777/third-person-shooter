// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoReloadDefault.h"

// Sets default values
AAmmoReloadDefault::AAmmoReloadDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	MagazineDrop = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Magazine Drop Mesh"));
	MagazineDrop->SetGenerateOverlapEvents(false);
	MagazineDrop->SetCollisionProfileName(TEXT("NoCollision"));
	MagazineDrop->SetupAttachment(RootComponent);
	
}

// Called when the game starts or when spawned
void AAmmoReloadDefault::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAmmoReloadDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAmmoReloadDefault::InitAmmo(UStaticMesh* MagazineDropped)
{
	MagazineDrop->SetStaticMesh(MagazineDropped);
	this->SetLifeSpan(10.0f);
}

