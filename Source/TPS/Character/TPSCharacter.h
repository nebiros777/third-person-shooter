// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FuncLibrary/Types.h"
#include "TPS/WeaponDefault.h"


#include "TPSCharacter.generated.h"



UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;
public:
	ATPSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent);
	

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

public:
	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);
	UDecalComponent* CurrentCursor = nullptr;

	//Weapon
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	AWeaponDefault* CurrentWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;

	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	EMovementState MovementState = EMovementState::Run_State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;
	//Inputs
	UFUNCTION()
		virtual void InputAxisX(float Value);
	UFUNCTION()
		virtual void InputAxisY(float Value);
	UFUNCTION()
		void InputAttackPressed();
		UFUNCTION()
			void InputAttackReleased();
		UFUNCTION(BlueprintCallable)
		void TryReloadweapon();
		UFUNCTION()
			void WeaponReloadStart(UAnimMontage* Anim);
		UFUNCTION()
			void WeaponReloadEnd();
		UFUNCTION(BlueprintNativeEvent)
			void WeaponReloadEnd_BP();
		UFUNCTION(BlueprintNativeEvent)
			void WeaponReloadStart_BP(UAnimMontage* Anim);

	UFUNCTION()
		virtual void MovementTick(float DeltaTime);
	UFUNCTION()
		virtual void ZoomTick(float DeltaTime);

	float AxisX = 0.0f;
	float AxisY = 0.0f;
	float ArmLenghtAfetrZoom = 1250.0f;
	
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);

	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState(EMovementState NewMovementState);
	UFUNCTION(BlueprintCallable)
		void ZoomByMouse(ZoomState NewZoomState);
	
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeapon);//FName IdWeapon
	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();

	//UFUNCTION(BlueprintCallable)
	//	UDecalComponent* GetCursorToWorld();


};

