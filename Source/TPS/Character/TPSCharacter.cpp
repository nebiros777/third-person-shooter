// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TPSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine.h"
#include "Game/TPSGameInstance.h"
#include "Engine/World.h"





ATPSCharacter::ATPSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	//CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	//CursorToWorld->SetupAttachment(RootComponent);
	//static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	//if (DecalMaterialAsset.Succeeded())  
	//{
	//	CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	//}
	//CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	//CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATPSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	//if (CursorToWorld != nullptr)
	//{
	//	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	//	{
	//		if (UWorld* World = GetWorld())
	//		{
	//			FHitResult HitResult;
	//			FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
	//			FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
	//			FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
	//			Params.AddIgnoredActor(this);
	//			World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
	//			FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
	//			CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
	//		}
	//	}
	//	else if (APlayerController* PC = Cast<APlayerController>(GetController()))
	//	{
	//		FHitResult TraceHitResult;
	//		PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
	//		FVector CursorFV = TraceHitResult.ImpactNormal;
	//		FRotator CursorR = CursorFV.Rotation();
	//		CursorToWorld->SetWorldLocation(TraceHitResult.Location);
	//		CursorToWorld->SetWorldRotation(CursorR);
	//	}
	//}
	if (CurrentCursor) {
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC) {
			FHitResult TraceHitResult;
					myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
					FVector CursorFV = TraceHitResult.ImpactNormal;
					FRotator CursorR = CursorFV.Rotation();
					CurrentCursor->SetWorldLocation(TraceHitResult.Location);
					CurrentCursor->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);
	ZoomTick(DeltaSeconds);
}

void ATPSCharacter::BeginPlay() {
	Super::BeginPlay();

	if (CursorMaterial) {
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}

	InitWeapon(InitWeaponName);
}


void ATPSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);
	
	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATPSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATPSCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATPSCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATPSCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATPSCharacter::TryReloadweapon);


}

void ATPSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATPSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATPSCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATPSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATPSCharacter::TryReloadweapon()
{
	if (CurrentWeapon)
	{
		if (CurrentWeapon->GetWeaponRound() <= CurrentWeapon->WeaponSettings.MaxRound)
		{
			CurrentWeapon->InitReload();
		}
	}
}

void ATPSCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATPSCharacter::WeaponReloadEnd()
{
	WeaponReloadEnd_BP();
}

void ATPSCharacter::WeaponReloadEnd_BP_Implementation()
{
	// in BP
}

void ATPSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void ATPSCharacter::MovementTick(float DeltaTime)
{	
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);
	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	
	if (myController)
	{
		//FHitResult ResultHit;
		//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);		
		//float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
		FVector WorldLocation, WorldDirection;
		
		myController->DeprojectMousePositionToWorld(WorldLocation, WorldDirection);
		WorldDirection = WorldDirection * 5000 + WorldLocation;
		float		T;
		FVector Intersection;
		float FindRotaterResultYaw = 0.0f;
		bool Go = UKismetMathLibrary::LinePlaneIntersection_OriginNormal(WorldLocation, WorldDirection, FVector(0, 0, 117), FVector(0, 0, 1), T, Intersection);
		if (Go)
		{
			FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Intersection).Yaw;
		}
		
		SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));

		if (CurrentWeapon)
		{
			FVector Displacement = FVector(0);
			switch (MovementState)
			{
			case EMovementState::Aim_State:
				Displacement = FVector(0.0f, 0.0f, 160.0f);
				CurrentWeapon->ShouldReduceDispersion = true;
				break;
			case EMovementState::Walk_State:
				Displacement = FVector(0.0f, 0.0f, 120.0f);
				CurrentWeapon->ShouldReduceDispersion = true;
				break;
			case EMovementState::Run_State:
				Displacement = FVector(0.0f, 0.0f, 120.0f);
				CurrentWeapon->ShouldReduceDispersion = false;
				break;
			default:
				break;
			}
			CurrentWeapon->ShootEndLocation = Intersection + Displacement;
		}
	}
	
	
	CharacterUpdate();

}

void ATPSCharacter::ZoomTick(float DeltaTime)
{
	float NewArmLenght = UKismetMathLibrary::FInterpTo(GetCameraBoom()->TargetArmLength,ArmLenghtAfetrZoom, DeltaTime,5);
	GetCameraBoom()->TargetArmLength = NewArmLenght;
}

void ATPSCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon) {
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - current weapon - NULL"));
}

void ATPSCharacter::CharacterUpdate()
{
	ACharacter* myCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	FRotator MyPlayerRotation = myCharacter->GetActorRotation();
	FRotator MoveRotation = myCharacter->GetVelocity().Rotation();
	float ResSpeed = 600.0f;
	float RunSpeed = 600.0f;

	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);	
		FVector WorldLocation, WorldDirection;
		if (myController)
		{
			myController->DeprojectMousePositionToWorld(WorldLocation, WorldDirection);
		}
		WorldDirection = WorldDirection * 5000 + WorldLocation;
	float		T;
	FVector Intersection;
	float FindRotaterResultYaw = 0.0f;
	bool Go = UKismetMathLibrary::LinePlaneIntersection_OriginNormal(WorldLocation, WorldDirection, FVector(0, 0, 117), FVector(0, 0, 1), T,Intersection);
	if (Go)
	{
		FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Intersection).Yaw;
		//GEngine->AddOnScreenDebugMessage(0, 1, FColor::Green, FString::SanitizeFloat(UKismetMathLibrary::FTrunc(MoveRotation.Yaw)));
		//GEngine->AddOnScreenDebugMessage(1, 1, FColor::Green, FString::SanitizeFloat(UKismetMathLibrary::FTrunc(FindRotaterResultYaw)));
	}
	else
	{
		//GEngine->AddOnScreenDebugMessage(0, 1, FColor::Green, TEXT("false"));
	}
	
	if ((UKismetMathLibrary::FTrunc(FindRotaterResultYaw / 10) == UKismetMathLibrary::FTrunc((MoveRotation.Yaw-1) / 10)) || (UKismetMathLibrary::FTrunc(FindRotaterResultYaw / 10) == -1*UKismetMathLibrary::FTrunc((MoveRotation.Yaw-1) / 10)))
	{
		RunSpeed = MovementInfo.RunSpeed;
	}
	else
	{
		RunSpeed = MovementInfo.WalkSpeed;
	}
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Run_State:
		
		ResSpeed = RunSpeed;
		break;
	case EMovementState::Walk_State:	
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	default:
		break;		
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATPSCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	CharacterUpdate();

	//weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon) {
		myWeapon->UpdateStateweapon(MovementState);
	}
}

void ATPSCharacter::ZoomByMouse(ZoomState NewZoomState)
{
	switch (NewZoomState)
	{
	case ZoomState::Zoom_Plus:
		ArmLenghtAfetrZoom = ArmLenghtAfetrZoom + 80;//GetCameraBoom()->TargetArmLength + 80;
		break;
	case ZoomState::Zoom_Minus:
		ArmLenghtAfetrZoom = ArmLenghtAfetrZoom - 80;//GetCameraBoom()->TargetArmLength - 80;
		break;
	default:
		break;
	}
	
}

void ATPSCharacter::InitWeapon(FName IdWeaponName)//FName IdWeapon
{
	FWeaponInfo myWeaponInfo;
	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetGameInstance());
	if (myGI) {
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass) {
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon) {
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;					

					myWeapon->WeaponSettings = myWeaponInfo;
					myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;
					myWeapon->UpdateStateweapon(MovementState);

					myWeapon->OnWeaponReloadStart.AddDynamic(this,&ATPSCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATPSCharacter::WeaponReloadEnd);
				}
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon not found in table"));
	}

}

AWeaponDefault* ATPSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}


