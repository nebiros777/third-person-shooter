// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"
#include "Kismet/GameplayStatics.h"
#include "Engine.h"
#include "Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();	

	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (GetWeaponRound() > 0)
	{	
		if (WeaponFiring)
		{
			if (FireTimer < 0.f)
			{
			
				if (!WeaponReloading)
				{
					Fire();
				}
				
			}
			else
			{
				FireTimer -= DeltaTime;
			}

		}
	}
	else
	{
		if (!WeaponReloading)
		{
			InitReload();
		}

	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading) {
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}

		if (CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	if (ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh) {
		SkeletalMeshWeapon->DestroyComponent(true);
	}
	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh()) {
		StaticMeshWeapon->DestroyComponent();
	}

	
	
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire()) {
		WeaponFiring = bIsFire;
	}
	else
		WeaponFiring = false;

}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return true;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSettings.ProjectileSettings;
}

void AWeaponDefault::Fire()
{
	FireTimer = WeaponSettings.RateOfFire;
	WeaponInfo.Round -= 1;
	ChangeDispersionByShot();

	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSettings.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSettings.EffectFireWeapon, ShootLocation->GetComponentTransform());

	int8 NumberProjectile = GetNumberProjectileByShot();

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector EndLocation;
		for (int8 i = 0; i < NumberProjectile; i++)// shotgun
		{
			EndLocation = GetFireEndLocation();
			
			FVector Dir = EndLocation - SpawnLocation;
			Dir.Normalize();

			FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
			SpawnRotation = myMatrix.Rotator();

			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParams.Owner = GetOwner();
			SpawnParams.Instigator = GetInstigator();

			if (ProjectileInfo.Projectile)
			{
				

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					myProjectile->InitialLifeSpan = 20.0f;
					myProjectile->InitProjectile(ProjectileInfo);
				}
					
				
			}
			else
			{
				//ToDo Projectile null Init trace fire
			}

			
		}

		
		
	}
}

void AWeaponDefault::UpdateStateweapon(EMovementState NewMovementState)
{
	BlockFire = false;
	switch (NewMovementState)
	{
	case EMovementState::Aim_State:
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::Walk_State:
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.Walk_StateDispersionReduction;
		break;
	case EMovementState::Run_State:
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.Run_StateDispersionReduction;
		break;
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion += CurrentDispersionRecoil;
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.0f);
	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);


	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = (ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(( ShootLocation->GetComponentLocation()- ShootEndLocation ).GetSafeNormal()));
		DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSettings.DistanceTrace, GetCurrentDispersion() * PI / 180.0f, GetCurrentDispersion() * PI / 180.0f, 32, FColor::Emerald, false, 0.1f, (uint8)'\000', 1.0f);
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSettings.DistanceTrace, GetCurrentDispersion() * PI / 180.0f, GetCurrentDispersion() * PI / 180.0f, 32, FColor::Emerald, false, 0.1f, (uint8)'\000', 1.0f);
	}


	
		
	/*if (ShowDebug)
	{
		//DirectionWeaponLook
		DrawDebugLine(GetWorld(),ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() )

	}*/
	
	return EndLocation;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{

	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
		
}

float AWeaponDefault::GetCurrentDispersion() const
{

	return CurrentDispersion;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return WeaponInfo.Round;
}

void AWeaponDefault::InitReload()
{

	WeaponReloading = true;
	ReloadTimer = WeaponSettings.ReloadTime;

	//ToDo Anim Reloading
	if (WeaponSettings.AnimCharReload)
	{
		OnWeaponReloadStart.Broadcast(WeaponSettings.AnimCharReload);
	}

	DoMagazineDrop();
}

void AWeaponDefault::DoMagazineDrop()
{
	FVector SpawnLocation = SkeletalMeshWeapon->GetComponentLocation();
	FRotator SpawnRotation = SkeletalMeshWeapon->GetComponentRotation();

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = GetOwner();
	SpawnParams.Instigator = GetInstigator();
	if (WeaponSettings.MagazineDrop && WeaponSettings.MagazineDropClass)
	{

		AAmmoReloadDefault* myMagazineDrop = Cast<AAmmoReloadDefault>(GetWorld()->SpawnActor(WeaponSettings.MagazineDropClass, &SpawnLocation, &SpawnRotation, SpawnParams));
		if (myMagazineDrop)
		{
			
			myMagazineDrop->InitAmmo(WeaponSettings.MagazineDrop);
		}
	}
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	WeaponInfo.Round = WeaponSettings.MaxRound;

	OnWeaponReloadEnd.Broadcast();
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSettings.NumberProjectileByShot;
}

